const express = require('express')
const Rcon = require('modern-rcon')
var bodyParser = require('body-parser')
const app = express()
const { WebClient } = require('@slack/client')
const port = 6060

const CHANNEL_ID = 'C06C2NHME'
const slack = new WebClient(process.env.SLACK_BOT_TOKEN)

const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next)
}

const rcon = new Rcon(process.env.MC_IP, 25575, process.env.PASSWORD)
const intervalRcon = new Rcon(process.env.MC_IP, 25575, process.env.PASSWORD)
let users = []

const helpText = `
How to use:

\`/minecraft whitelist <username>\`, 
`

app.use(bodyParser.urlencoded({ extended: true }))

setInterval(() => {
  try {
    intervalRcon
      .connect()
      .then(() => {
        return intervalRcon.send('list')
      })
      .then(res => {
        if (res.startsWith('There are ')) {
          const currentUsers = res
            .split(':')[1]
            .split(' ')
            .map(n => n.replace(',', ''))
          currentUsers.forEach(cu => {
            if (!users.includes(cu) && cu.trim()) {
              // say they joined
              slack.chat.postMessage({
                channel: CHANNEL_ID,
                text: `*${cu}* has joined the game`,
              })
            }
          })
          users.forEach(u => {
            if (!currentUsers.includes(u) && u.trim()) {
              slack.chat.postMessage({
                channel: CHANNEL_ID,
                text: `*${u}* has left the game`,
              })
            }
          })
          users = currentUsers
          return users
        }
      })
      .then(() => {
        intervalRcon.disconnect()
      })
  } catch (e) {
    console.error(e)
  }
}, 1800000)

app.post(
  '/',
  asyncMiddleware(async (req, res) => {
    const text = req.body.text
    const args = text.split(' ')
    const command = args.length > 0 ? args[0] : null
    if (command === 'whitelist') {
      await rcon
        .connect()
        .then(() => {
          return rcon.send(`whitelist add ${args[1]}`) // That's a command for Minecraft
        })
        .catch(reason => {
          res.send('oops something went wrong :angry-cry:')
        })
        .then(res => {
          console.log(res)
        })
        .then(() => {
          return rcon.disconnect()
        })

      res.send({
        response_type: 'in_channel',
        text: `whitelisted ${args[1]}`,
      })
    }
    if (command === 'list' || command === 'status') {
      const t = await rcon
        .connect()
        .then(() => {
          return rcon.send(`list`)
        })
        .catch(reason => {
          res.send('oops something went wrong :angry-cry:')
        })
        .then(res => {
          console.log(res)
          rcon.disconnect()
          return res
        })

      res.send({
        response_type: 'in_channel',
        text: t,
      })
    } else {
      res.send(helpText)
    }
  })
)

app.listen(port, () => console.log(`Minecraft app listening on port ${port}!`))
